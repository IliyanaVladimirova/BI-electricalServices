import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import '../styles/index.css';

import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

class NavRout extends Component {

    constructor() {
        super();
    }
    render() {
        return (
            <div>
                <div className="nav-container">
                    <nav className="navbar navbar-expand-lg navbar-light ">
                        <a className="navbar-brand" href="/">B.I
                <div>Electrical services <span className="icon-power"></span></div>
                        </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="navdrop collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div className="navbar-nav">                           
                                    <Link to="/" className="nav-item nav-link">Начало</Link>
                                    <Link to="/about" className="nav-item nav-link">За мен</Link>                             
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        )
    }
}

export default NavRout;
