import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import '../styles/index.css';
import '../styles/main.css'

class Prices extends Component {
    render() {
        return (
            <div id="prices-services">
                <h5>{this.props.serviceName}</h5>
                <p>{this.props.price}</p>
            </div>
        )
    }
}
export default Prices;