import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import '../styles/index.css';
import '../styles/main.css'

import PriceList from './PriceList'
import Prices from './Prices'

class MapPrices extends Component {
    render() {
        const serviceComponents = PriceList.map(price =>
            <Prices
                key={price.id} serviceName={price.seviceType} price={price.price}
            />)
        return (
            <div>
                {serviceComponents}
            </div>
        )
    }
}
export default MapPrices;