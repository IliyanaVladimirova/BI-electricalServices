import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import '../styles/index.css';


import CarouselImg from '../images/electropic1.jpg';
import CabelsImg from '../images/cables.jpg'
import ElectricianImg from '../images/electrician.jpg'

class Carousel extends Component{
    render(){
        return(
            <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
            <div className="carousel-inner">
                <div className="carousel-item active">
                    <img className="d-block w-100" src={ElectricianImg} alt="First slide" />
                    <div className="carousel-caption">
                        <h3>Бързо и качествено</h3>
                        <p>извършване на електро услуги</p>
                    </div>
                </div>
                <div className="carousel-item">
                    <img className="d-block w-100" src={CabelsImg} alt="Second slide" />
                    <div className="carousel-caption">
                        <h3>Коректно отношение</h3>
                        <p>Etiam magna enim,
                        bibendum et elit sit amet</p>
                    </div>
                </div>
                <div className="carousel-item">
                    <img className="d-block w-100" src={CarouselImg} alt="Third slide" />
                    <div className="carousel-caption">
                        <h3>Etiam magna enim,</h3>
                        <p>bibendum et elit sit amet</p>
                    </div>
                </div>
            </div>
            <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="sr-only">Next</span>
            </a>
        </div>
        )
    }
}

export default Carousel;