import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import '../styles/index.css';
import '../styles/footer.css'

class Footer extends Component {
    render() {
        var date = new Date();
        var year = date.getFullYear();
        return (
            <footer>
                <div id="banner-call">
                    <span>Електроуслуги по домовете</span>
                    <span>Обадете ни се!</span>
                </div>
                <div className="f-contain fluid">
                    <div className="logo">
                        <h4 className="navbar-brand">B.I Electrical services <span className="icon-power"></span></h4>
                    </div>
                    <div className="contact-details">
                        <p> <span className="icon-mobile"></span>0895723001</p>
                        <p><span className="icon-mail"></span> kjdhfkj@kasfj.ksfj</p>
                        <p> <span className="icon-clock"></span>
                    Понеделник-Петък: 18:00-00:00</p>
                    </div>
                </div>
                <div id="f-bottom">
                    <span>&copy; {year}</span>
                    <span>Гарантирано качество и сигурност!</span>
                </div>
            </footer>
        )
    }
}
export default Footer;