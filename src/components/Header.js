import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import '../styles/index.css'
import '../styles/header.css'
import '../styles/ico-style.css'

class Header extends Component{
    render(){
        return(
            <header>
                <div id="work-hours">
                    <p> <span className="icon-mobile"></span>0895723001</p>
                    <p> <span className="icon-clock"></span>
                    Понеделник-Петък: 18:00-00:00</p>
                </div>
            </header>
        )
    }
}
export default Header;