import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import '../styles/index.css';
import '../styles/contacts.css';

import CircuitImg from '../images/line-circuit.jpg'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

class Contacts extends Component {
    render() {
        return (
            <div className="c-wrapper">
                <img src={CircuitImg} className="img-fluid" alt="Circuit img" />
                <h5 className="centered text-c">Обадете ни се или заявете дата и час за посещение и оглед.</h5>
                <div id="btn-w">
                    {/* <button type="button" className="btn btn-secondary btn-sm"><span className="icon-power"></span>Връзка с нас</button> */}
                        <hr/>
                    <button component={Link} to="/about" type="button" className="btn btn-secondary btn-sm">
                        <span className="icon-power"></span>Направете запитване</button>
                </div>
            </div>
        )
    }
}

export default Contacts; 
