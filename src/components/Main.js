import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import '../styles/index.css'
import '../styles/main.css'

import Services from '../components/Services'
import ServicesData from '../components/ServicesData'
import MapPrices from './MapPrices'
import Carousel from './Carousel'

import ElectricianImg from '../images/el-guy.jpg';

class Main extends Component {
    // ---------------------------------------------------------
    constructor(props) {
        super();
        this.state = {
            isClicked: false
        }
        this.handleCLick = this.handleCLick.bind(this)
    }

    handleCLick(e) {
        e.preventDefault();
        this.setState(prevState => {
            return {
                isClicked: !prevState.isClicked
            }
        })
    }

    // ---------------------------------------------------------
    render() {
        const serviceComponents = ServicesData.map(service =>
            <Services
                key={service.id} imgUrl={service.imgUrl} 
                serviceName={service.serviceName} 
                serviceDescr={service.serviceDescr}
            />)
        return (
            <div>
                <Carousel />
                <main id="main-container">
                    <div id="section-container">
                        <section id="about">
                            <h1>Професионално извършване на електоуслуги</h1>
                            <div className="about-wrapper fluid">
                                <figure>
                                    <img src={ElectricianImg} alt="electrician img" />
                                </figure>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Mauris tempus massa quis lacus faucibus,
                                    in sodales nibh dapibus. Etiam magna enim,
                                    bibendum et elit sit amet, ultricies eleifend felis.
                                    Cras interdum venenatis accumsan. Aenean ac lectus ac
                                    arcu faucibus tincidunt pretium vel nunc.
                            </p>
                            </div>
                        </section>
                        <section id="offered-services">
                            <h1>Част от предлаганите от нас услуги са: </h1>
                            <div className="srvc-container fluid">
                                {serviceComponents}
                            </div>
                            <div className="btn-w">
                                <a className="more-btn" href="">
                                    <button type="button" className="btn btn-secondary btn-sm"
                                        onClick={this.handleCLick} >
                                        <span className="icon-power"></span> Всички услуги + цени</button>
                                </a>
                            </div>
                        </section>
                        {/* -------------------------------------- */}
                        {
                            this.state.isClicked ?
                                <MapPrices /> :
                                null
                        }
                        {/* -------------------------------------- */}
                    </div>
                </main>
            </div>
        )
    }
}
export default Main;