const PriceList = [
        {
            id:1,
            seviceType:"Mонтаж на електротабла",
            price:"40лв."
        },
        {
            id:2,
            seviceType:"Mонтаж на ел. ключове и контакти",
            price:"5лв."
        },
        {
            id:3,
            seviceType:"Mонтаж на осветителни тела",
            price:"15лв."
        },
        {
            id:4,
            seviceType:"Ремонт на стълбищно осветление",
            price:"30лв."
        },
        {
            id:5,
            seviceType:"Проверка на инсталация",
            price:"30лв."
        }
]

export default PriceList;