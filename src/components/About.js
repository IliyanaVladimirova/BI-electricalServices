import React, { Component } from 'react'
import '../styles/about.css'

class About extends Component {
    constructor() {
        super();
        this.state = {
            imgPath: "https://picsum.photos/300"
        }
    }
    render() {
        return (
            <div>
                <section id="myInfo">
                    <h2>За мен</h2>
                    <img src={this.state.imgPath} />
                    <p>Etiam magna enim,
                    bibendum et elit sit amet, ultricies eleifend felis.
                    Cras interdum venenatis accumsan. Aenean ac lectus ac
                    arcu faucibus tincidunt pretium vel nunc.</p>
                </section>
            </div>
        )
    }
}

export default About;