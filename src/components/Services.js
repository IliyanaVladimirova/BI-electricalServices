import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import '../styles/index.css';
import '../styles/main.css'

class Services extends Component {
    render() {
        return (
            <div id="services">
                <img src={this.props.imgUrl} />
                <h3>{this.props.serviceName}</h3>
                <p>{this.props.serviceDescr}</p>
            </div>
        )
    }
}
export default Services;