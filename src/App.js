import React, { Component } from 'react';

import Main from './components/Main'
import Header from './components/Header'
import Footer from './components/Footer'
import Contacts from './components/Contacts'
import NavRout from './components/NavRout'
import About from './components/About' 

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

class App extends Component {

  render() {
    return (
      <Router>
      <div>
        <Header />
        <NavRout />
        <Switch>
            <Route path={"/"} exact component={Main} />
            <Route path="/about" component={About} />
        </Switch>
        <Contacts />
        <Footer />
      </div >
      </Router>
    )
  }

}

export default App;
